﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="8" MadCap:lastHeight="856" MadCap:lastWidth="648">
    <head>
        <link href="../Resources/TableStyles/DetailedwithPadding.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1 MadCap:autonum="1. &#160;">Multi-Object Delete</h1>
        <p style="text-align: left;">The Multi-Object Delete operation enables the deletion of multiple objects from a bucket using a single HTTP request. If object keys to be deleted are known, this operation provides a suitable alternative to sending individual delete requests, reducing per-request overhead.  Refer to <MadCap:xref href="DELETE Object.htm">"DELETE Object" on page 1</MadCap:xref>.</p>
        <p style="text-align: left;">The Multi-Object Delete request contains a list of up to 1000 keys that can be deleted. In the XML, provide the object key names. Optionally, provide version ID to delete a specific version of the object from a versioning-enabled bucket. For each key, <MadCap:variable name="S3 Connector Variables.ComponentName" /> performs a delete operation and returns the result of that delete, success or failure, in the response. Note that, if the object specified in the request is not found, <MadCap:variable name="S3 Connector Variables.ComponentName" /> returns the result as deleted.</p>
        <p style="text-align: left;">The Multi-Object Delete operation supports two modes for the response—verbose and quiet. By default, the operation uses verbose mode in which the response includes the result of deletion of each key in the request. In quiet mode the response includes only keys where the delete operation encountered an error. For a successful deletion, the operation does not return any information about the delete in the response body.</p>
        <p MadCap:conditions="PrintGuides.Future" style="text-align: left;">When performing a Multi-Object Delete operation on an MFA Delete enabled bucket that attempts to delete any versioned objects, an MFA token must be included. If such a token is not provided the entire request will fail, even if an attempt is being made to delete non-versioned objects. Also, in the event that an invalid token is provided (regardless of whether there are versioned keys in the request or not), the entire Multi-Object Delete request will fail. For information about MFA Delete, see MFA Delete.</p>
        <p style="text-align: left;">Finally, the Content-MD5 header is required for all Multi-Object Delete requests. Amazon S3 uses the header value to ensure that your request body has not be altered in transit.</p>
        <h2 class="NoTOCentry" MadCap:autonum=" ">Requests</h2>
        <h3 MadCap:autonum="1.0.1 &#160;">Request Syntax</h3><pre xml:space="preserve">POST / ?delete HTTP/1.1
Host: {{BucketName}}.{{StorageService}}.com
Authorization: {{authorizationString}}
Content-Length: {{length}}
Content-MD5: {{MD5}}

&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;Delete&gt;
    &lt;Quiet&gt;true&lt;/Quiet&gt;
    &lt;Object&gt;
         &lt;Key&gt;Key&lt;/Key&gt;
         &lt;VersionId&gt;VersionId&lt;/VersionId&gt;
    &lt;/Object&gt;
    &lt;Object&gt;
         &lt;Key&gt;Key&lt;/Key&gt;
    &lt;/Object&gt;
    ...
&lt;/Delete&gt;</pre>
        <h3 MadCap:autonum="1.0.2 &#160;">Request Parameters</h3>
        <p style="text-align: left;">The Multi-Object Delete operation requires a single query string parameter called "delete" to distinguish it from other bucket POST operations.</p>
        <h3 MadCap:autonum="1.0.3 &#160;">Request Headers</h3>
        <p style="text-align: left;"> The Multi-Object Delete operation uses two Request Headers —  <span class="ElementName">Content-MD5</span>, and <span class="ElementName">Content-Length</span> — in addition to those that are common to all operations (refer to <MadCap:xref href="../7_Request Headers/Request Headers.htm#Common">"Common Request Headers" on page 1</MadCap:xref>). </p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 108px;" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Header</th>
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Content-MD5</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">The base64-encoded 128-bit MD5 digest of the data. This header must be used as a message integrity check to verify that the request body was not corrupted in transit.</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">Content-Length</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">string</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        <p class="InTableFormatted">Length of the body according to RFC 2616.</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h3 MadCap:autonum="1.0.4 &#160;">Request Elements</h3>
        <p style="text-align: left;">The Multi-Object Delete operation can request the following items:</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 108px;" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Element</th>
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Delete</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Container</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Container for the request</p>
                        <p class="InTableFormatted">Ancestor: None</p>
                        <p class="InTableFormatted">Children: One or more <span class="ElementName">Object</span> elements and an optional <span class="ElementName">Quiet</span> element</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Quiet</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Boolean</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Element to enable quiet mode for the request (when added, the element must be set to true)</p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">Delete</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Object</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Container</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Elementat that describes the delete request for an object</p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">Delete</span></p>
                        <p class="InTableFormatted">Children: <span class="ElementName">Key</span> element and an optional <span class="ElementName">VersionId</span> element</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Key</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">String</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Key name of the object to delete</p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">Object</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">VersionId</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">String</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        <p class="InTableFormatted">VersionId for the specific version of the object to delete</p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">Object</span></p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h2 class="NoTOCentry" MadCap:autonum=" ">Responses </h2>
        <h3 MadCap:autonum="1.0.5 &#160;">Response Headers</h3>
        <p class="pgBreakKeepNext" style="text-align: left;">Implementation of the Multi-Object Delete operation uses only response headers that are common to all operations (refer to <MadCap:xref href="../8_Response Headers/Response Headers.htm">"Response Headers" on page 1</MadCap:xref>).</p>
        <h3 MadCap:autonum="1.0.6 &#160;">Response Elements</h3>
        <p style="text-align: left;">The Multi-Object Delete operation can return the following XML elements of the response:</p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Element</th>
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Type</th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">DeleteResult</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Container</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Container for the response</p>
                        <p class="InTableFormatted">Ancestor: None</p>
                        <p class="InTableFormatted">Children: <span class="ElementName">Deleted</span>, <span class="ElementName">Error</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Deleted</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Container</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p>Container element for a successful delete (identifies the object that was successfully deleted)</p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">DeleteResult</span></p>
                        <p>Children: <span class="ElementName">Key</span>, <span class="ElementName">VersionId</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Key</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">String</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p>Key name for the object that Amazon S3 attempted to delete</p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">Deleted</span>, <span class="ElementName">Error</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">VersionId</td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">String</td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p>Version ID of the versioned object <MadCap:variable name="S3 Connector Variables.ComponentName"></MadCap:variable> attempted to delete. <MadCap:variable name="S3 Connector Variables.ComponentName"></MadCap:variable> Includes this element only in case of a versioned-delete request.</p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">Deleted</span> or <span class="ElementName">Error</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">DeleteMarker</td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">Boolean</td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p>DeleteMarker element with a true value indicates that the request accessed a delete marker.

If a specific delete request either creates or deletes a delete marker, this element is returned in the response with a value of true. This is the case only when your Multi-Object Delete request is on a bucket that has versioning enabled or suspended.

</p>
                        <p>Ancestor: <span class="ElementName">Deleted</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">DeleteMarkerVersionId</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">String</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p>Version ID of the delete marker accessed (deleted or created) by the request.</p>
                        <p>If the specific delete request in the Multi-Object Delete either creates or deletes a delete marker, <MadCap:variable name="S3 Connector Variables.ComponentName" /> returns this element in response with the version ID of the delete marker. When deleting an object in a bucket with versioning enabled, this value is present for the following two reasons:</p>
                        <ul>
                            <li style="font-family: Calibri;font-size: 9pt;">A non-versioned delete request is sent, that is, only the object key is specified and not the version ID. In this case, <MadCap:variable name="S3 Connector Variables.ComponentName" /> creates a delete marker and returns its version ID in the response.</li>
                            <li style="font-family: Calibri;font-size: 9pt;">A versioned delete request is sent, that is, an object key and a version ID are specified in therequest; however, the version ID identifies a delete marker. In this case, <MadCap:variable name="S3 Connector Variables.ComponentName" /> deletes the delete marker and returns the specific version ID in response.</li>
                        </ul>
                        <p class="InTableFormatted" style="font-family: Calibri;font-size: 9pt;">Ancestor: <span class="ElementName">Deleted</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Error</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">String</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p style="text-align: left;">Container for a failed delete operation that describes the object that <MadCap:variable name="S3 Connector Variables.ComponentName" /> attempted to delete and the error it encountered.</p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">DeleteResult</span></p>
                        <p>Children: <span class="ElementName">Key</span>, <span class="ElementName">VersionId</span>, <span class="ElementName">Code</span>, <span class="ElementName">Message</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Key</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">String</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p>Key for the object <MadCap:variable name="S3 Connector Variables.ComponentName" /> attempted to delete</p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">Error</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Code</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">String</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p>Status code for the result of the failed delete</p>
                        <p>Valid Values: <span class="Code_Terminal">AccessDenied</span>, <span class="Code_Terminal">InternalError</span></p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">Error</span></p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1" MadCap:conditions="">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">Message</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">String</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        <p>Error description</p>
                        <p class="InTableFormatted">Ancestor: <span class="ElementName">Error</span></p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h2 class="NoTOCentry" MadCap:autonum=" ">Examples</h2>
        <h3 MadCap:autonum="1.0.7 &#160;">Multi-Object Delete Resulting in Mixed Success/Error Response</h3>
        <p style="text-align: left;">The request sample illustrates a Multi-Object Delete request to delete objects that result in mixed success and errors response. </p>
        <h4>Request Sample</h4>
        <p style="text-align: left;">The request deletes two objects from {{bucketname}} (in this example, the requester does not have permission to delete the sample2.txt object).</p><pre xml:space="preserve" style="page-break-inside: avoid;">POST /?delete HTTP/1.1
Host: {{bucketname}}.s3.scality.com
Accept: */*
x-amz-date: Wed, 12 Oct 2009 17:50:00 GMT
Content-MD5: p5/WA/oEr30qrEE121PAqw==
Authorization: {{authorizationString}}
Content-Length: {{length}}
Connection: Keep-Alive</pre><pre xml:space="preserve">

&lt;Delete&gt;
  &lt;Object&gt;
    &lt;Key&gt;sample1.txt&lt;/Key&gt;
  &lt;/Object&gt;
  &lt;Object&gt;
    &lt;Key&gt;sample2.txt&lt;/Key&gt;
  &lt;/Object&gt;
&lt;/Delete&gt;</pre>
        <h4>Response Sample</h4>
        <p style="text-align: left;">The response includes a <span class="ElementName">DeleteResult</span> element that includes a <span class="ElementName">Deleted</span> element for the item that <MadCap:variable name="S3 Connector Variables.ComponentName" /> successfully deleted and an <span class="ElementName">Error</span> element that <MadCap:variable name="S3 Connector Variables.ComponentName" /> did not delete because the userdidn't have permission to delete the object.</p>
        <p class="codeparatext" style="color: #000000;">HTTP/1.1 200 OK</p>
        <p class="codeparatext" style="color: #000000;">x-amz-id-2: 5h4FxSNCUS7wP5z92eGCWDshNpMnRuXvETa4HH3LvvH6VAIr0jU7tH9kM7X+njXx</p>
        <p class="codeparatext" style="color: #000000;">x-amz-request-id: A437B3B641629AEE</p>
        <p class="codeparatext" style="color: #000000;">Date: Fri, 02 Dec 2011 01:53:42 GMT</p>
        <p class="codeparatext" style="color: #000000;">Content-Type: application/xml</p>
        <p class="codeparatext" style="color: #000000;">Server: ScalityS3</p>
        <p class="codeparatext_lastline">Content-Length: 251</p>
        <p class="codeparatext">&lt;?xml version="1.0" encoding="UTF-8"?&gt;</p>
        <p class="codeparatext">&lt;DeleteResult xmlns="http://s3.amazonaws.com/doc/2006-03-01/"&gt;</p>
        <p class="codeparatext_indent">&lt;Deleted&gt;</p>
        <p class="codeparatext_indent_indent">&lt;Key&gt;sample1.txt&lt;/Key&gt;</p>
        <p class="codeparatext_indent">&lt;/Deleted&gt;</p>
        <p class="codeparatext_indent">&lt;Error&gt;</p>
        <p class="codeparatext_indent_indent">&lt;Key&gt;sample2.txt&lt;/Key&gt;</p>
        <p class="codeparatext_indent_indent">&lt;Code&gt;AccessDenied&lt;/Code&gt;</p>
        <p class="codeparatext_indent_indent">&lt;Message&gt;Access Denied&lt;/Message&gt;</p>
        <p class="codeparatext_indent">&lt;/Error&gt;</p>
        <p class="codeparatext">&lt;/DeleteResult&gt;</p>
        <h3 MadCap:autonum="1.0.8 &#160;">Deleting Object from a Versioned Bucket</h3>
        <p style="text-align: left;">In deleting an item from a versioning enabled bucket, all versions of that object remain in the bucket; however, <MadCap:variable name="S3 Connector Variables.ComponentName" /> inserts a delete marker.</p>
        <p style="text-align: left;">The following scenarios describe the behavior of a Multi-Object Delete request when versioning is enabled for a bucket.</p>
        <h4>Scenario 1: Simple Delete</h4>
        <p style="text-align: left;">As shown, the Multi-Object Delete request specifies only one key.</p><pre xml:space="preserve">POST /?delete HTTP/1.1
Host: {{bucketname}}.s3.scality.com
Accept: */*
x-amz-date: Wed, 30 Nov 2011 03:39:05 GMT
Content-MD5: p5/WA/oEr30qrEEl21PAqw==
Authorization: {{authorizationString}}
Content-Length: {{length}}
Connection: Keep-Alive

&lt;Delete&gt;
  &lt;Object&gt;
    &lt;Key&gt;SampleDocument.txt&lt;/Key&gt;
  &lt;/Object&gt;
&lt;/Delete&gt;</pre>
        <p style="text-align: left;">As versioning is enabled on the bucket, <MadCap:variable name="S3 Connector Variables.ComponentName" /> does not delete the object, instead adding a delete marker. The response indicates that a delete marker was added (the <span class="ElementName">DeleteMarker</span> element in the response has a value of true) and the version number of the added delete marker.</p><pre xml:space="preserve">HTTP/1.1 201 OK
x-amz-id-2: P3xqrhuhYxlrefdw3rEzmJh8z5KDtGzb+/FB7oiQaScI9Yaxd8olYXc7d1111ab+
x-amz-request-id: 264A17BF16E9E80A
Date: Wed, 30 Nov 2011 03:39:32 GMT
Content-Type: application/xml
Server: ScalityS3
Content-Length: 276

&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;DeleteResult xmlns="http://s3.scality.com/doc/2006-03-01/"&gt;
  &lt;Deleted&gt;
    &lt;Key&gt;SampleDocument.txt&lt;/Key&gt;
    &lt;DeleteMarker&gt;true&lt;/DeleteMarker&gt; 
    &lt;DeleteMarkerVersionId&gt;NeQt5xeFTfgPJD8B4CGWnkSLtluMr11s&lt;/DeleteMarkerVersionId&gt;
  &lt;/Deleted&gt;
&lt;/DeleteResult&gt;</pre>
        <h4>Scenario 2: Versioned Delete</h4>
        <p style="text-align: left;">As shown, the Multi-Object Delete attempts to delete a specific version of an object.</p>
        <p class="codeparatext" style="color: #000000;text-align: left;">POST /?delete HTTP/1.1</p>
        <p class="codeparatext" style="color: #000000;">Host: {{bucketname}}.s3.scality.com</p>
        <p class="codeparatext" style="color: #000000;">Accept: */*</p>
        <p class="codeparatext" style="color: #000000;">x-amz-date: Wed, 30 Nov 2011 03:39:05 GMT</p>
        <p class="codeparatext" style="color: #000000;">Content-MD5: p5/WA/oEr30qrEEl21PAqw==</p>
        <p class="codeparatext" style="color: #000000;">Authorization: {{authorizationString}}</p>
        <p class="codeparatext" style="color: #000000;">Content-Length: {{length}}</p>
        <p class="codeparatext_lastline">Connection: Keep-Alive</p>
        <p class="codeparatext">&lt;Delete&gt;</p>
        <p class="codeparatext_indent">&lt;Object&gt;</p>
        <p class="codeparatext_indent_indent">&lt;Key&gt;sampledocument.txt&lt;/Key&gt;</p>
        <p class="codeparatext_indent_indent">&lt;VersionId&gt;OYcLXagmS.WaD..oyH4KRguB95_YhLs7&lt;/VersionId&gt;</p>
        <p class="codeparatext_indent">&lt;/Object&gt;</p>
        <p class="codeparatext_lastline">&lt;/Delete&gt;</p>
        <p style="text-align: left;">In this case, <MadCap:variable name="S3 Connector Variables.ComponentName" /> deletes the specific object version from the bucket and returns the following response. In the response, <MadCap:variable name="S3 Connector Variables.ComponentName" /> returns the key and version ID of the deleted object.</p>
        <p class="codeparatext" style="color: #000000;">HTTP/1.1 201 OK</p>
        <p class="codeparatext" style="color: #000000;">x-amz-id-2: P3xqrhuhYxlrefdw3rEzmJh8z5KDtGzb+/FB7oiQaScI9Yaxd8olYXc7d1111xx+</p>
        <p class="codeparatext" style="color: #000000;">x-amz-request-id: 264A17BF16E9E80A</p>
        <p class="codeparatext" style="color: #000000;">Date: Wed, 30 Nov 2011 03:39:32 GMT</p>
        <p class="codeparatext" style="color: #000000;">Content-Type: application/xml</p>
        <p class="codeparatext" style="color: #000000;">Server: ScalityS3</p>
        <p class="codeparatext_lastline">Content-Length: 219</p>
        <p class="codeparatext">&lt;?xml version="1.0" encoding="UTF-8"?&gt;</p>
        <p class="codeparatext">&lt;DeleteResult xmlns="http://s3.scality.com/doc/2006-03-01/"&gt;</p>
        <p class="codeparatext_indent">&lt;Deleted&gt;</p>
        <p class="codeparatext_indent_indent">&lt;Key&gt;sampledocument.txt&lt;/Key&gt;</p>
        <p class="codeparatext_indent_indent">&lt;VersionId&gt;OYcLXagmS.WaD..oyH4KRguB95_YhLs7&lt;/VersionId&gt;</p>
        <p class="codeparatext_indent">&lt;/Deleted&gt;</p>
        <p class="codeparatext_lastline">&lt;/DeleteResult&gt;</p>
        <h4>Scenario 3: Versioned Delete of a Delete Marker</h4>
        <p style="text-align: left;">In the preceding example, the request refers to a delete marker (in lieu of an object), then <MadCap:variable name="S3 Connector Variables.ComponentName" /> deletes the delete marker. The effect of this operation is to make the object reappear in the bucket. The response returned by <MadCap:variable name="S3 Connector Variables.ComponentName" /> indicates the deleted delete marker (<span class="ElementName">DeleteMarker</span> element with value true) and the version ID of the delete marker.</p>
        <p class="codeparatext" style="color: #000000;">HTTP/1.1 200 OK</p>
        <p class="codeparatext" style="color: #000000;">x-amz-id-2: IIPUZrtolxDEmWsKOae9JlSZe6yWfTye3HQ3T2iAe0ZE4XHa6NKvAJcPp51zZaBr</p>
        <p class="codeparatext" style="color: #000000;">x-amz-request-id: D6B284CEC9B05E4E</p>
        <p class="codeparatext" style="color: #000000;">Date: Wed, 30 Nov 2011 03:43:25 GMT</p>
        <p class="codeparatext" style="color: #000000;">Content-Type: application/xml</p>
        <p class="codeparatext" style="color: #000000;">Server: ScalityS3</p>
        <p class="codeparatext_lastline">Content-Length: {{length}}</p>
        <p class="codeparatext">&lt;?xml version="1.0" encoding="UTF-8"?&gt;</p>
        <p class="codeparatext">&lt;DeleteResult xmlns="http://s3.scalitys3.com/doc/2006-03-01/"&gt;</p>
        <p class="codeparatext_indent">&lt;Deleted&gt;</p>
        <p class="codeparatext_indent_indent">&lt;Key&gt;sampledocument.txt&lt;/Key&gt;</p>
        <p class="codeparatext_indent_indent">&lt;VersionId&gt;NeQt5xeFTfgPJD8B4CGWnkSLtluMr11s&lt;/VersionId&gt;</p>
        <p class="codeparatext_indent_indent">&lt;DeleteMarker&gt;true&lt;/DeleteMarker&gt;</p>
        <p class="codeparatext_indent_indent">&lt;DeleteMarkerVersionId&gt;NeQt5xeFTfgPJD8B4CGWnkSLtluMr11s&lt;/DeleteMarkerVersionId&gt;</p>
        <p class="codeparatext_indent">&lt;/Deleted&gt;</p>
        <p class="codeparatext_lastline">&lt;/DeleteResult&gt;</p>
        <p style="text-align: left;">In general, when a Multi-Object Delete request results in <MadCap:variable name="S3 Connector Variables.ComponentName" /> either adding a delete marker or removing a delete marker, the response returns the following elements:</p>
        <p class="codeparatext">&lt;DeleteMarker&gt;true&lt;/DeleteMarker&gt;</p>
        <p class="codeparatext">&lt;DeleteMarkerVersionId&gt;NeQt5xeFTfgPJD8B4CGWnkSLtluMr11s&lt;/DeleteMarkerVersionId&gt;</p>
        <h3 MadCap:autonum="1.0.9 &#160;">Malformed XML in the Request</h3>
        <p style="text-align: left;">The request sample sends a malformed XML document (missing the Delete end element).</p>
        <h4>Request Sample</h4>
        <p class="codeparatext">POST /?delete HTTP/1.1</p>
        <p class="codeparatext">Host: bucketname.S3.amazonaws.com</p>
        <p class="codeparatext">Accept: */*</p>
        <p class="codeparatext">x-amz-date: Wed, 30 Nov 2011 03:39:05 GMT</p>
        <p class="codeparatext">Content-MD5: p5/WA/oEr30qrEEl21PAqw==</p>
        <p class="codeparatext">Authorization: AWS AKIAIOSFODNN7EXAMPLE:W0qPYCLe6JwkZAD1ei6hp9XZIee=</p>
        <p class="codeparatext">Content-Length: 104</p>
        <p class="codeparatext_lastline">Connection: Keep-Alive</p>
        <p class="codeparatext">&lt;Delete&gt;</p>
        <p class="codeparatext_indent">&lt;Object&gt;</p>
        <p class="codeparatext_indent_indent">&lt;Key&gt;404.txt&lt;/Key&gt;</p>
        <p class="codeparatext_indent">&lt;/Object&gt;</p>
        <p class="codeparatext_indent">&lt;Object&gt;</p>
        <p class="codeparatext_indent_indent">&lt;Key&gt;a.txt&lt;/Key&gt;</p>
        <p class="codeparatext_indent">&lt;/Object&gt;</p>
        <h4>Response Sample</h4>
        <p style="text-align: left;">The response returns the Error messages that describe the error.</p>
        <p class="codeparatext">HTTP/1.1 200 OK</p>
        <p class="codeparatext">x-amz-id-2: P3xqrhuhYxlrefdw3rEzmJh8z5KDtGzb+/FB7oiQaScI9Yaxd8olYXc7d1111ab+</p>
        <p class="codeparatext">x-amz-request-id: 264A17BF16E9E80A</p>
        <p class="codeparatext">Date: Wed, 30 Nov 2011 03:39:32 GMT</p>
        <p class="codeparatext">Content-Type: application/xml</p>
        <p class="codeparatext">Server: AmazonS3</p>
        <p class="codeparatext_lastline">Content-Length: 207</p>
        <p class="codeparatext">&lt;?xml version="1.0" encoding="UTF-8"?&gt;</p>
        <p class="codeparatext">&lt;Error&gt;</p>
        <p class="codeparatext_indent">&lt;Code&gt;MalformedXML&lt;/Code&gt;</p>
        <p class="codeparatext_indent">&lt;Message&gt;The XML you provided was not well-formed or did not validate against our published schema&lt;/Message&gt;</p>
        <p class="codeparatext_indent">&lt;RequestId&gt;264A17BF16E9E80A&lt;/RequestId&gt;</p>
        <p class="codeparatext_indent">&lt;HostId&gt;P3xqrhuhYxlrefdw3rEzmJh8z5KDtGzb+/FB7oiQaScI9Yaxd8olYXc7d1111ab+&lt;/HostId&gt;</p>
        <p class="codeparatext">&lt;/Error&gt;</p>
    </body>
</html>