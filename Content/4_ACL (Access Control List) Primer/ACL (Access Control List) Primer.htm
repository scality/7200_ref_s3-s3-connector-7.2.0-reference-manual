﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="6" MadCap:lastHeight="854" MadCap:lastWidth="650">
    <head>
        <link href="../Resources/TableStyles/DetailedwithPadding.css" rel="stylesheet" MadCap:stylesheetType="table" />
        <link href="../Resources/TableStyles/SimpleDefinitions.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1 MadCap:autonum="1. &#160;">ACL (Access Control List)</h1>
        <p style="text-align: left;">Access Control Lists (ACLs) enable the managing of access to buckets and objects. </p>
        <p style="text-align: left;">Each bucket and object has an ACL attached to it as a subresource, defining which accounts or groups are granted access and the type of access. When a request is received against a resource, <MadCap:variable name="S3 Connector Variables.ComponentName" /> checks the corresponding ACL to verify the requester has the necessary access permissions.</p>
        <p style="text-align: left;">When a bucket or object is created, <MadCap:variable name="S3 Connector Variables.ComponentName" /> creates a default ACL that grants the resource owner full control over the resource as shown in the following sample bucket ACL (the default object ACL has the same structure).</p><pre xml:space="preserve">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;AccessControlPolicy xmlns="http://s3.scality.com/doc/2006-03-01/"&gt;
  &lt;Owner&gt;
    &lt;ID&gt;*** Owner-Canonical-User-ID ***&lt;/ID&gt;
    &lt;DisplayName&gt;owner-display-name&lt;/DisplayName&gt;
  &lt;/Owner&gt;
  &lt;AccessControlList&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
               xsi:type="Canonical User"&gt;
        &lt;ID&gt;*** Owner-Canonical-User-ID ***&lt;/ID&gt;
        &lt;DisplayName&gt;display-name&lt;/DisplayName&gt;
      &lt;/Grantee&gt;
      &lt;Permission&gt;FULL_CONTROL&lt;/Permission&gt;
    &lt;/Grant&gt;
  &lt;/AccessControlList&gt;
&lt;/AccessControlPolicy&gt; </pre>
        <p style="text-align: left;">The sample ACL includes an <span class="ElementName">Owner</span> element identifying the owner via the account's canonical user ID. The Grant element identifies the grantee (either a specific account or a predefined group), and the permission granted. This default ACL has one Grant element for the owner. You grant permissions by adding Grant elements, each grant identifying the grantee and the permission.</p>
        <h2 MadCap:autonum="1.1. &#160;">Grantee Eligibility</h2>
        <p style="text-align: left;">A grantee can be an account or one of the predefined groups. Permission is granted to an account by the email address or the canonical user ID. However, if an email address is provided in the grant request, <MadCap:variable name="S3 Connector Variables.ComponentName" /> finds the canonical user ID for that account and adds it to the ACL. The resulting ACLs will always contain the canonical user ID for the account, not the account's email address.</p>
        <h3 MadCap:autonum="1.1.1 &#160;">AWS Canonical User ID</h3>
        <p style="text-align: left;">Canonical user IDs are associated with AWS accounts. When an individual AWS account is granted permissions by a grant request, a grant entry is added to the ACL with that account's canonical user ID.</p>
        <h3 MadCap:autonum="1.1.2 &#160;">Predefined Amazon S3 Groups</h3>
        <p class="pgBreakKeepNext" style="text-align: left;"><MadCap:variable name="S3 Connector Variables.ComponentName" /> offers the use of Amazon S3 predefined groups. When granting account access to such a group, specify one of URIs instead of a canonical user ID. </p>
        <table style="mc-table-style: url('../Resources/TableStyles/SimpleDefinitions.css');width: 100%;" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 120pt;" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 427px;" />
            <tbody>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                        <p class="InTableFormatted">Authenticated Users</p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">
                        <p class="InTableFormatted">Represents all authenticated accounts. Access permission to this group allows any system account to access the resource. However, all requests must be signed (authenticated).</p>
                        <p>http://acs.amazonaws.com/groups/global/AuthenticatedUsers</p>
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                        <p class="InTableFormatted">Public</p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">
                        <p class="InTableFormatted">Access permission to this group allows anyone to access the resource. The requests can be signed (authenticated) or unsigned (anonymous). Unsigned requests omit the Authentication header in the request.</p>
                        <p>http://acs.amazonaws.com/groups/global/AllUsers</p>
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyB-Column1-Row1">
                        <p class="InTableFormatted">Log Delivery</p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyA-Column1-Row1">
                        <p class="InTableFormatted">WRITE permission on a bucket enables this group to write server access logs to the bucket.</p>
                        <p>http://acs.amazonaws.com/groups/s3/LogDelivery</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="Note" style="text-align: left;">When using ACLs, a grantee can be an AWS account or one of the predefined Amazon S3 groups. However, the grantee cannot be an Identity and Access Management (IAM) user.</p>
        <p class="Note" style="text-align: left;">When granting AWS accounts access to resources, be aware that the AWS accounts can delegate their permissions to users under their accounts (a practice  known as cross-account access).</p>
        <h2 MadCap:autonum="1.2. &#160;">Grantable Permissions</h2>
        <p style="text-align: left;page-break-after: avoid;">The set of permissions <MadCap:variable name="S3 Connector Variables.ComponentName" /> supports in an ACL are detailed in the following table. </p>
        <p class="Note">The set of ACL permissions is the same for object ACL and bucket ACL. However, depending on the context (bucket ACL or object ACL), these ACL permissions grant permissions for specific bucket or the object operations. </p>
        <table style="width: 100%;mc-table-style: url('../Resources/TableStyles/DetailedwithPadding.css');" class="TableStyle-DetailedwithPadding" cellspacing="0">
            <col class="TableStyle-DetailedwithPadding-Column-Column1" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 250px;" />
            <col class="TableStyle-DetailedwithPadding-Column-Column1" style="width: 250px;" />
            <thead>
                <tr class="TableStyle-DetailedwithPadding-Head-Header1">
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">Permission</th>
                    <th class="TableStyle-DetailedwithPadding-HeadE-Column1-Header1">When Granted to a Bucket</th>
                    <th class="TableStyle-DetailedwithPadding-HeadD-Column1-Header1">When Granted to an Object</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">READ</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Grantee can list the objects in the bucket</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Grantee can read the object data and its metadata</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">WRITE</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Grantee can create, overwrite, and delete any object in the bucket</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Not applicable</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">READ_ACP</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Grantee can read the bucket ACL</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Grantee can read the object ACL</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">WRITE_ACP</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyE-Column1-Body1">
                        <p class="InTableFormatted">Grantee can write the ACL for the applicable bucket</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyD-Column1-Body1">
                        <p class="InTableFormatted">Grantee can write the ACL for the applicable object</p>
                    </td>
                </tr>
                <tr class="TableStyle-DetailedwithPadding-Body-Body1">
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">FULL_CONTROL</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyB-Column1-Body1">
                        <p class="InTableFormatted">Allows grantee the READ, WRITE, READ_ACP, and WRITE_ACP permissions on the bucket</p>
                    </td>
                    <td class="TableStyle-DetailedwithPadding-BodyA-Column1-Body1">
                        <p class="InTableFormatted">Allows grantee the READ, READ_ACP, and WRITE_ACP permissions on the object</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h2 MadCap:autonum="1.3. &#160;">Specifying an ACL</h2>
        <p class="pgBreakKeepNext" style="text-align: left;">Using <MadCap:variable name="S3 Connector Variables.ComponentName" />, an ACL can be set at the creation point of a bucket or object. An ACL can also be applied to an existing bucket or object.</p>
        <table style="mc-table-style: url('../Resources/TableStyles/SimpleDefinitions.css');width: 100%;" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 120pt;" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 427px;" />
            <tbody>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">
                        <p class="InTableFormatted">Set ACL using request headers</p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1">
                        <p class="InTableFormatted">When sending a request to create a resource (bucket or object), set an ACL using the request headers. With these headers, it is possible to either specify a canned ACL or specify grants explicitly (identifying grantee and permissions explicitly).</p>
                    </td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyB-Column1-Row1">
                        <p class="InTableFormatted">Set ACL using request body</p>
                    </td>
                    <td class="TableStyle-SimpleDefinitions-BodyA-Column1-Row1">
                        <p class="InTableFormatted">When you send a request to set an ACL on a existing resource, you can set the ACL either in the request header or in the body.</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <h2 class="NoTOCentry" MadCap:autonum=" ">Sample ACL</h2>
        <p style="text-align: left;">The ACL on a bucket identifies the resource owner and a set of grants. The format is the XML representation of an ACL in the <MadCap:variable name="S3 Connector Variables.ComponentName" /> API. The bucket owner has FULL_CONTROL of the resource. In addition, the ACL shows how permissions are granted on a resource to two accounts, identified by canonical user ID, and two of the predefined Amazon S3 groups.</p><pre xml:space="preserve">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;AccessControlPolicy xmlns="http://s3.scality.com/doc/2006-03-01/"&gt;
  &lt;Owner&gt;
    &lt;ID&gt;Owner-canonical-user-ID&lt;/ID&gt;
    &lt;DisplayName&gt;display-name&lt;/DisplayName&gt;
  &lt;/Owner&gt;
  &lt;AccessControlList&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;
        &lt;ID&gt;Owner-canonical-user-ID&lt;/ID&gt;
        &lt;DisplayName&gt;display-name&lt;/DisplayName&gt;
      &lt;/Grantee&gt;
      &lt;Permission&gt;FULL_CONTROL&lt;/Permission&gt;
    &lt;/Grant&gt;
    
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;
        &lt;ID&gt;user1-canonical-user-ID&lt;/ID&gt;
        &lt;DisplayName&gt;display-name&lt;/DisplayName&gt;
      &lt;/Grantee&gt;
      &lt;Permission&gt;WRITE&lt;/Permission&gt;
    &lt;/Grant&gt;

    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="CanonicalUser"&gt;
        &lt;ID&gt;user2-canonical-user-ID&lt;/ID&gt;
        &lt;DisplayName&gt;display-name&lt;/DisplayName&gt;
      &lt;/Grantee&gt;
      &lt;Permission&gt;READ&lt;/Permission&gt;
    &lt;/Grant&gt;

    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Group"&gt;
        &lt;URI&gt;http://acs.amazonaws.com/groups/global/AllUsers&lt;/URI&gt; 
      &lt;/Grantee&gt;
      &lt;Permission&gt;READ&lt;/Permission&gt;
    &lt;/Grant&gt;
    &lt;Grant&gt;
      &lt;Grantee xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Group"&gt;
        &lt;URI&gt;http://acs.amazonaws.com/groups/s3/LogDelivery&lt;/URI&gt;
      &lt;/Grantee&gt;
      &lt;Permission&gt;WRITE&lt;/Permission&gt;
    &lt;/Grant&gt;

  &lt;/AccessControlList&gt;
&lt;/AccessControlPolicy&gt;</pre>
    </body>
</html>